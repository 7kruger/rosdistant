<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Book</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <header style="position: absolute">
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
            <div class="container-fluid">
                <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/books">Главная страница</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="d-flex flex-wrap justify-content-center align-content-center flex-column h-100 w-100">
        <div style="width: 300px;">
            <div class="mb-2">
                <input class="form-control" type="text" name="title" placeholder="Название" value="${book.title}">
            </div>
            <div class="mb-2">
                <textarea class="form-control" type="text" name="description" placeholder="Описание">${book.description}</textarea>
            </div>
            <div class="mb-2">
                <input class="form-control" type="text" name="author" placeholder="Автор" value="${book.author}">
            </div>
            <div class="d-flex justify-content-around">
                <button class="btn btn-primary" type="submit" onclick="updateBook()">Сохранить</button>
                <button class="btn btn-danger" type="submit" onclick="deleteBook()">Удалить</button>
            </div>
        </div>
    </div>

    <script>
        const id = ${book.getId()}

        async function updateBook() {
            const book = {
                id: id,
                title: document.querySelector('input[name="title"]').value,
                description: document.querySelector('textarea[name="description"]').value,
                author: document.querySelector('input[name="author"]').value,
            }

            const response = await fetch('books', {
                method: 'put',
                body: JSON.stringify(book)
            });

            if (response.ok) {
                window.location.replace('/books')
            } else {
                alert('Не удалось применить изменения')
            }
        }

        async function deleteBook() {
            const response = await fetch(`books/${id}`, {
                method: 'delete'
            })

            if (response.ok) {
                window.location.replace('/books')
            } else {
                alert('Не удалось удалить книгу')
            }
        }
    </script>
</body>
</html>
