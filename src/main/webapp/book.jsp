<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${book.title}</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
            <div class="container-fluid">
                <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/books">Главная страница</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="p-4">
        <div><b>Название: </b>${book.title}</div>
        <div><b>Описание: </b>${book.description}</div>
        <div><b>Автор: </b>${book.author}</div>
        <div class="mt-2">
            <a class="btn btn-secondary" href="/books/${book.id}/edit">Редактировать</a>
        </div>
    </div>
</body>
</html>
