<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список книг</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
            <div class="container-fluid">
                <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/books">Главная страница</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <c:if test="${books.size() > 0}">
        <div class="p-4">
            <div>
                <a class="btn btn-success position-absolute" href="/books/create">Добавить</a>
                <h3 class="text-center mb-3">Список книг</h3>
            </div>
            <div class="list-group">
                <c:forEach var="book" items="${books}">
                    <a class="list-group-item list-group-item-action list-group-item-primary" href="/books/${book.id}">${book.title}</a>
                </c:forEach>
            </div>
        </div>
    </c:if>
    <c:if test="${books.size() <= 0}">
        <div class="d-flex justify-content-center flex-column w-100">
            <h1 class="text-center"><%= "Список книг пуст" %></h1>
            <div class="d-flex justify-content-center">
                <a class="btn btn-success" href="/books/create">Добавить</a>
            </div>
        </div>
    </c:if>
</body>
</html>
