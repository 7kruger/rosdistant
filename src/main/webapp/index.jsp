<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>rosdistant</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <style>
    body, html {
      height: 100%;
      margin: 0;
      /*display: flex;*/
      /*justify-content: center;*/
      /*align-items: center;*/
    }
  </style>
</head>
<body>
  <header style="position: absolute">
    <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
      <div class="container-fluid">
        <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
          <ul class="navbar-nav flex-grow-1">
            <li class="nav-item">
              <a class="nav-link text-dark" href="/books">Главная страница</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <div class="d-flex justify-content-center flex-column w-100" style="height: 100%">
    <h1 class="text-center"><%= "Районная библиотека" %></h1>
    <div class="d-flex justify-content-center">
      <a class="btn btn-primary" href="/books">Перейти</a>
    </div>
  </div>
</body>
</html>