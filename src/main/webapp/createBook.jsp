<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Book</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        body, html {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        form {
            width: 300px;
        }
    </style>
</head>
<body>
    <header style="position: absolute">
        <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
            <div class="container-fluid">
                <div class="navbar-collapse collapse d-sm-inline-flex justify-content-between">
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/books">Главная страница</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="d-flex flex-wrap justify-content-center align-content-center flex-column h-100 w-100">
        <form action="/books" method="post">
            <div class="mb-2">
                <input class="form-control" type="text" name="title" placeholder="Название">
            </div>
            <div class="mb-2">
                <textarea class="form-control" type="text" name="description" placeholder="Описание"></textarea>
            </div>
            <div class="mb-2">
                <input class="form-control" type="text" name="author" placeholder="Автор">
            </div>
            <div class="d-flex justify-content-center">
                <button class="btn btn-primary" type="submit">Создать</button>
            </div>
        </form>
    </div>
</body>
</html>
