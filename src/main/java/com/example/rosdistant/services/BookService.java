package com.example.rosdistant.services;

import com.example.rosdistant.models.Book;
import com.example.rosdistant.repository.BookRepository;
import java.util.List;

public class BookService {
    private BookRepository repository;

    public BookService() {
        repository = new BookRepository();
    }

    public List<Book> getAllBooks() {
        return repository.getAll();
    }

    public Book getBookById(long id) {
        return repository.getById(id);
    }

    public boolean createBook(Book book) {
        return repository.create(book);
    }

    public boolean updateBook(Book book) {
        return repository.update(book);
    }

    public boolean deleteBook(long id) {
        return repository.delete(id);
    }
}
