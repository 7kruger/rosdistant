package com.example.rosdistant.controllers;

import com.example.rosdistant.models.Book;
import com.example.rosdistant.services.BookService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import com.google.gson.Gson;

@WebServlet("/books/*")
public class BookController extends HttpServlet {
    private BookService bookService = new BookService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            List<Book> books = bookService.getAllBooks();

            request.setAttribute("books", books);
            request.getRequestDispatcher("/books.jsp").forward(request, response);
        } else {
            String[] pathParts = pathInfo.split("/");
            if (pathParts.length > 1) {
                if ("create".equals(pathParts[1])) {
                    request.getRequestDispatcher("/createBook.jsp").forward(request, response);
                } else if (pathParts.length > 2 && "edit".equals(pathParts[2])) {
                    long id = Long.parseLong(pathParts[1]);
                    Book book = bookService.getBookById(id);

                    if (book != null) {
                        request.setAttribute("book", book);
                        request.getRequestDispatcher("/editBook.jsp").forward(request, response);
                    } else {
                        request.setAttribute("message", "Книга не найдена");
                        request.getRequestDispatcher("/error.jsp").forward(request, response);
                    }
                } else {
                    long id = Long.parseLong(pathParts[1]);
                    Book book = bookService.getBookById(id);

                    if (book != null) {
                        request.setAttribute("book", book);
                        request.getRequestDispatcher("/book.jsp").forward(request, response);
                    } else {
                        request.setAttribute("message", "Книга не найдена");
                        request.getRequestDispatcher("/error.jsp").forward(request, response);
                    }
                }
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Книга не найдена");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String author = request.getParameter("author");

        Book book = new Book(title, description, author);
        boolean created = bookService.createBook(book);

        if (created) {
            response.sendRedirect(request.getContextPath() + "/books");
        } else {
            request.setAttribute("message", "Не удалось создать книгу");
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder requestBody = new StringBuilder();
        String line;
        try (BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null) {
                requestBody.append(line);
            }
        }
        String body = requestBody.toString();

        Gson gson = new Gson();
        Book book = gson.fromJson(body, Book.class);

        if (book.getTitle() == null || book.getDescription() == null || book.getAuthor() == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write("Missing fields in request body");
            return;
        }

        boolean updated = bookService.updateBook(book);

        if (updated) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] pathParts = request.getPathInfo().split("/");
        long id = Long.parseLong(pathParts[1]);

        boolean deleted = bookService.deleteBook(id);

        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
